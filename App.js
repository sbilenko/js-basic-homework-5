/*
1. Метод об'єкту - це функція, за допомогою якої ми виконуємо якісь дії. Функція є властивістю об'єкту.
2. Значення властивості об'єкту може мати тип Boolean, String, Number, function()...будь що.
3. Це означає, що, якщо ми присвоюємо об'єкту якусь змінну, то ця змінна це тільки посилання на об'єкт(на певне місце в пам'яті). Ми можемо присвоїти скільки завгодно змінних, які будуть посилатись на один об'єкт. 
*/

// ВАРІАНТ 1 + доп завдання

// Створення нового юзера

const createNewUser = () => {
  let firstName = prompt('Введіть ваше ім"я')
  let lastName = prompt('Введіть ваше прізвище')
  return { firstName, lastName }
}

const newUser = createNewUser()

// Додавання методів

newUser.getLogin = function () {
  return (
    this.firstName.toLowerCase().substring(0, 1) + this.lastName.toLowerCase()
  )
}

newUser.setFirstName = function (newFirstName) {
  return Object.defineProperty(this, 'firstName', {
    value: newFirstName || this.firstName,
  })
}

newUser.setLastName = function (newLastName) {
  return Object.defineProperty(this, 'lastName', {
    value: newLastName || this.lastName,
  })
}

// Налаштування методів

Object.defineProperties(newUser, {
  firstName: {
    writable: false,
    configurable: true,
  },
  lastName: {
    writable: false,
    configurable: true,
  },
  getLogin: {
    enumerable: false,
    writable: false,
  },
  setFirstName: {
    enumerable: false,
    writable: false,
  },
  setLastName: {
    enumerable: false,
    writable: false,
  },
})

console.log(newUser)
console.log('Виклик метода getLogin - ', newUser.getLogin())











// // ВАРІАНТ 2 + доп завдання

// // Створення нового юзера з перевірками на правильність написання

// const createNewUser = () => {
//   let firstName = prompt('Введіть ваше ім"я')
//   while (!firstName || !isNaN(firstName)) {
//     firstName = prompt('Введіть ваше ім"я')
//   }
//   let lastName = prompt('Введіть ваше прізвище')
//   while (!lastName || !isNaN(lastName)) {
//     lastName = prompt('Введіть ваше прізвище')
//   }
//   return { firstName, lastName }
// }

// const newUser = createNewUser()

// // Створення нових методів і їх налаштування 

// Object.defineProperties(newUser, {
//   firstName: {
//     enumerable: true,
//     writable: false,
//   },
//   lastName: {
//     enumerable: true,
//     writable: false,
//   },
//   getLogin: {
//     value: function () {
//       return (
//         this.firstName.toLowerCase().substring(0, 1) +
//         this.lastName.toLowerCase()
//       )
//     },
//   },
//   setFirstName: {
//     value: function (newFirstName) {
//       return Object.defineProperty(this, 'firstName', {
//         value: newFirstName || this.firstName,
//       })
//     },
//   },
//   setLastName: {
//     value: function (newLastName) {
//       return Object.defineProperty(this, 'lastName', {
//         value: newLastName || this.lastName,
//       })
//     },
//   },
// })

// console.log(newUser)
// console.log('Виклик метода getLogin - ', newUser.getLogin())
